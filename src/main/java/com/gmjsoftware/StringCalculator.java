package com.gmjsoftware;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.join;

public class StringCalculator {

    public int add(String delimitedNumbers) {
        if (delimitedNumbers.isBlank()) return 0;

        var numbers = convertToIntegerList(delimitedNumbers);

        validate(numbers);

        return calculateTotal(numbers);
    }

    private void validate(List<Integer> numbers) {
        var negativeNumbers = numbers.stream().filter(i -> i < 0)
                .map(String::valueOf).collect(Collectors.toList());

        if (!negativeNumbers.isEmpty()) {
            throw new IllegalArgumentException("Negatives not allowed: " + join(", ", negativeNumbers));
        }
    }

    private int calculateTotal(List<Integer> numbers) {
        return numbers.stream()
                .mapToInt(i -> i)
                .sum();
    }

    private List<Integer> convertToIntegerList(String delimitedNumbers) {
        var result = new ArrayList<Integer>();

        var delim = getDelims(delimitedNumbers);
        var trimmedDelimitedNumbers = trimAnyCustomDelimPrefix(delimitedNumbers);

        var stringNumbers = trimmedDelimitedNumbers.split(delim);

        for (var stringNumber : stringNumbers) {
            result.add(Integer.parseInt(stringNumber));
        }
        return result;
    }

    private String trimAnyCustomDelimPrefix(String delimitedNumbers) {
        var result = delimitedNumbers;
        if (delimitedNumbers.
                startsWith("//")) {

            result = delimitedNumbers.substring(delimitedNumbers.indexOf("\n") + 1);
        }
        return result;
    }

    private String getDelims(String delimitedNumbers) {
        var delim = ",|\\n";
        if (delimitedNumbers.startsWith("//")) {
            delim = delimitedNumbers.substring(2, 3);
        }
        return delim;
    }
}
