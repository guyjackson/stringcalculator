package gmjsoftware;

import com.gmjsoftware.StringCalculator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class StringCalculatorTest {

    private StringCalculator stringCalculator;

    @BeforeEach
    void setUp() {
        stringCalculator = new StringCalculator();
    }

    @Test
    void empty_string_returns_zero() {
        var result = stringCalculator.add("");
        assertThat(result).isEqualTo(0);
    }

    @ParameterizedTest
    @CsvSource({"'1', 1",
                "'1,2', 3",
                "'1,2\n3', 6"})
    void adds_provided_numbers(String numberString, int expectedTotal) {
        var result = stringCalculator.add(numberString);
        assertThat(result).isEqualTo(expectedTotal);
    }

    @ParameterizedTest
    @CsvSource({"'//;\n1;2', 3",
                "'//-\n1-2-3', 6"})
    void custom_delimiter_can_be_provided(String numbersString, int expectedTotal) {
        var result = stringCalculator.add(numbersString);
        assertThat(result).isEqualTo(expectedTotal);
    }

    @ParameterizedTest
    @CsvSource({"'1,-2', -2",
                "'1,-2,-3', '-2, -3'"})
    void negative_numbers_are_not_supported(String numbersString, String expectedErrorToContain) {

        assertThatThrownBy(() -> stringCalculator.add(numbersString))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Negatives not allowed: " + expectedErrorToContain);
    }
}
